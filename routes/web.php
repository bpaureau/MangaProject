<?php

use App\Http\Controllers\AdminCategorieController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\AdminProduitController;
use App\Http\Controllers\ClientCommandeController;
use App\Http\Controllers\ClientLivreController;
use App\Http\Controllers\ClientPanierController;
use App\Http\Controllers\ContactUsFormController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource("/catalogue", ClientLivreController::class)->only(["index", "show"])->parameter("catalogue","livre");

Route::middleware("auth")->group(function () {
    //ICI les routes des personnes loggées
    Route::resource("/panier",ClientPanierController::class)->except("create")->parameter("panier", "livre");
    Route::delete("/panier", [ClientPanierController::class, "vider"]);
    Route::resource("/commandes", ClientCommandeController::class);
});

Route:: get('/deconnexion', [Controller::class, 'deconnexion']);

Route::middleware("OnlyAdmin")->group(function () {
    //ICI les routes des admins
    Route::resource("admin/produits", AdminProduitController::class);
    Route::resource("admin/categories", AdminCategorieController::class);
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get("/contact", [ContactUsFormController::class, "afficheForm"]);
Route::post("/contact", [ContactUsFormController::class, "traitementForm"]);
Route::get('/reload-captcha', [ContactUsFormController::class, "reloadCaptcha"]);