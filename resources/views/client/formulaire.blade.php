@extends("layouts.app")
@section("titre")
Formulaire de contact
@endsection
@section("content")


<div class="container container-fluid mx-auto pt-3 container-contact-form">
<div class="col-sm-10 col-md-6 col-lg-4 mx-auto">
<h1 class="my-1">Formulaire de contact</h1>
<form action="/contact" method="post">
@csrf
<div class='row mb-2'>
    <label for='email'>Email *</label>
        <input value='{{old("email")}}' name='email' required type='email' class="form-control" id="email" placeholder="Entrer votre email">
    @error('email')
        <div class='alert alert-danger mt-1'>{{$message}}</div>
    @enderror
</div>
<div class='row mb-2'>
    <label for='sujet'>sujet *</label>
        <input value='{{old("sujet")}}' name='sujet' required type='text' class="form-control" id="sujet" placeholder="Entrer votre sujet">
    @error('sujet')
        <div class='alert alert-danger mt-1'>{{$message}}</div>
    @enderror
</div>
<div class='row mb-2'>
    <label for='message'>message *</label>
        <input value='{{old("message")}}' name='texte' required type='text' class="form-control" id="message" placeholder="Entrer votre message">
    @error('message')
        <div class='alert alert-danger mt-1'>{{$message}}</div>
    @enderror
</div>
<div class="row mb-2">
    <div class="captcha">
        <span>{!! captcha_img() !!}</span>
        <button type="button" class="btn btn-danger" class="reload" id="reload">
            &#x21bb;
        </button>
    </div>
<div class="form-group mb-4">
    <input id="captcha" type="text" class="form-control" placeholder="Entrer Captcha" name="captcha">
</div>

<button type="submit" class="btn btn-primary">Submit</button>
</div>
</form>
</div>
<div class="col-sm-1 col-md-1 col-xl-1 waves" style="background-image: url('{{ asset('/Images/backgroundnippon.png')}}');"></div>
</div>
@endsection