@extends("layouts.app")
@section("titre")
{{ $unLivre->nom }}
@endsection
@section("content")

<h1 class="text-center pb-3"> {{ Str::ucfirst($unLivre->nom) }}</h1>

<section>

</section>

<section>
    @guest
        <div class="alert alert-warning">
            Pour ajouter l'artcile à votre panier il faut vous <a href="/login">connecter</a> <br>
            Ou bien vous <a href="/register"> Creer un compte</a>
        </div>
    @endguest
    @auth

<div class="container microwave">
<div class="col-12 col-sm-10 col-md-6 col-lg-4 mx-auto">
<h1 class="my-1"></h1>
<form action="/panier/{{ $unLivre->id }}" method="POST">
@csrf
@method('put')
<div class='row mb-2 pb-4 mayo'>
    <label for='quantite'>Quantite *</label>
        <input value='1'min="1" name='quantite' required type='number' class="form-control" id="quantite" placeholder="Entrer quantite">
    @error('quantite')
        <div class='alert alert-danger mt-1'>{{$message}}</div>
    @enderror
</div>
<center><button type="submit" class="animated-link"><span> Commander </span></button></center>
</form>
</div>
</div>
@endauth
</section>

<section>

</section>


@endsection