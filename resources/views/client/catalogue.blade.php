<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

     <!-- CSRF Bootstrap -->
     <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">


    <!-- Styles -->


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"/>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        @include('layouts.nav')

        <main class="ketchup">
            <div class="row row-fluid d-flex japonais" style="background-image: url('{{ asset('/Images/145.jpg')}}');">
                    {{$lesLivres->links()}}
                    @foreach ($lesLivres as $unLivre )
                    <div class="card col-xs-12 col-sm-12 col-md-6 col-xl-2 card-tome">
                        <div class="card-header2">
                            <img src="Images/{{$unLivre->imageURL}}" class="card-img-top img-fluid img-card" alt="Image du tome">
                        </div>
                        <div class="card-body card-catalogue-body">
                            <h3 class="card-title card-catalogue-title">{{$unLivre->nom}}</h3>
                            <div class="container-fluid d-flex justify-content-center">
                                <p class="card-text">{{$unLivre->description}}</p>
                            </div>
                            <div class="container-fluid d-flex justify-content-center">
                                <h3 class="card-text card-catalogue-price">{{$unLivre->prix}}€</h3>
                            </div>
                            <div class="container-fluid d-flex justify-content-center">
                                <a href="/catalogue/{{$unLivre->id}}" class="animated-link-catalogue my-2"><span>Acheter</span></a>
                            </div>
                        </div>
                    </div>
                    @endforeach

                        {{$lesLivres->links()}}

                    </div>
            </div>
            @yield('content')
        </main>
    </div>



    @include('layouts.footer')