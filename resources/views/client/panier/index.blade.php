@extends("layouts.app")
@section("titre")
Mon panier
@endsection
@section("content")
<h1>Mon panier</h1>
<a href="/panier/create"  class="btn btn-success">Ajouter</a>
<table class="table table-responsive table-striped ">
    <thead>
        <th>Nom</th>
        <th>Prix unitaire</th>
        <th>Quantité</th>
        <th>Sous-total</th>
        <th>Actions</th>
    </thead>
<tbody>
@foreach ($panier as $ligne )
    <tr>
        <td>{{ $ligne->name }}</td>
        <td>{{ $ligne->price }}</td>
        <td>{{ $ligne->quantity }}</td>
        <td>{{ $ligne->quantity * $ligne->price }}</td>

        <td>
            <a href="/panier/{{$ligne->id}}/edit"  class="animated-link mb-2"><span> Modifier </span></a>
            <form action="/panier/{{$ligne->id}}" method="post">
            @method("delete")
            @csrf
            <button class="animated-link"><span> Supprimer </span></button>
            </form>
        </td>
    </tr>
@endforeach
<tr>

    <td>Total</td>
    <td></td>
    <td></td>
    <td>{{Cart::getTotal() }} €</td>
</tr>
</tbody>
</table>
<div class="container-fluid valider-commande">
    <form action="/panier" method="post">
        @csrf
        @method('delete')
        <button class="animated-link" id="viderlepanier"><span> Vider le panier </span></button>
    </form>
<form action="/commande" method="post">
    @csrf

    <button class="animated-link2"><span> Paiement </span></button>
</form>
</div>
@endsection