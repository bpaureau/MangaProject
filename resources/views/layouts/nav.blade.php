

{{--FINITO PIPO--}}

    <nav>
      <input type="checkbox" id="check">
      <label for="check" class="checkbtn">
        <i class="fas fa-bars"></i>
      </label>
      <label class="logo">&lt; Hello Mangas' World &gt;</label>
        <ul>
        @auth


        @if( auth()->user()->role->nom=="admin")
            <li>
                <div class="bouton2 ">
                    <a href="/admin/produits" class="animated-link">
                        <span>Produits</span>
                    </a>
                </div>
            </li>

            <li>
                <div class="bouton2 ">
                    <a href="/admin/categories" class="animated-link">
                        <span>Categories</span>
                    </a>
                </div>
            </li>
        @endauth

        @if( auth()->user()->role->nom=="client")
            <li>
                <div class="bouton3">
                    <span>Chantal <i class="bi bi-person-circle"></i></span>
                </div>
            </li>


        @endauth
        @endif
        @if(Request::url() === 'http://127.0.0.1:8000/catalogue')
            <li>
                <div class="container container-fluid containerBAR">
                    <form action="#" method="get">

                        <input
                            name="nom"
                            class="form-control searchBar"
                            placeholder="Recherche"
                            type="search"
                            value="{{request("nom")}}"
                        >
                    </form>
                </div>
            </li>
        @endif
            <li>
                <div class="bouton2 ">
                    <a href="/" class="animated-link">
                        <span>Home</span>
                    </a>
                </div>
            </li>
            <li>
                <div class="bouton2 ">
                    <a href="/catalogue" class="animated-link">
                        <span>Catalogue</span>
                    </a>
                </div>
            </li>
            <li>
                <div class="bouton2 ">
                    <a href="/contact" class="animated-link">
                        <span>Contact</span>
                    </a>
                </div>
            </li>
            <li>
                <div class="bouton2 ">
                    <a href="/panier" class="animated-link">
                        <span>Mon panier</span>
                    </a>
                </div>
            </li>
        @if( auth()->user())
            <li>
                <div class="bouton2 ">
                    <a href="/deconnexion" class="animated-link">
                        <span>Logout</span>
                    </a>
                </div>
            </li>
        @else
            <li>
                <div class="bouton2 ">
                    <a href="/deconnexion" class="animated-link">
                        <span>Login</span>
                    </a>
                </div>
            </li>

        @endif
    </ul>
</nav>


