
<footer class="footer">
    <div class="container container-fluid footer-container">
      <div class="row">
        <div class="col-xs-6 col-md-3 cacahuete">
          <h6>Contact</h6>
          <ul class="footer-links">
            <li><a href="">Contact@hmw.com</a></li>
            <li><a href="">01 xx xx xx xx</a></li>
          </ul>
        </div>

        <div class="col-xs-6 col-md-3 cacahuete">
          <h6>Hello Mangas' World</h6>
          <ul class="footer-links">
            <li><a href="">CGV</a></li>
            <li><a href="">Mentions Légales</a></li>
            <li><a href="">Politique de confidentialité</a></li>

          </ul>
        </div>
        <div class="col-xs-6 col-md-3 cacahuete">
            <h6>Socials</h6>
            <ul class="footer-links">
              <li><i class="bi bi-emoji-sunglasses"><a href=""> Paureau Baptiste</a></i></li>
              <li><i class="bi bi-facebook"><a href=""> Facebook</a></i></li>
              <li><i class="bi bi-instagram"><a href=""> Instagram</a></i></li>
              <li><i class="bi bi-discord"><a href=""> Discord</a></i></li>

            </ul>
          </div>
      </div>
    </div>
</footer>
@yield("scripts")
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
</body>
</html>
