<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

     <!-- CSRF Bootstrap -->
     <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">


    <!-- Styles -->


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"/>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        @include('layouts.nav')

        <main class="ketchup">

            <div class="row row-fluid d-flex">
                <div class="col col-sm-1 col-md-1 col-xl-1 waves" style="background-image: url('{{ asset('/Images/backgroundnippon.png')}}');"></div>
                <div class="col col-sm-10 col-md-10 col-xl-10 container-fluid nippon" style="background-image: url('{{ asset('/Images/koifish.jpg')}}');">
                    <h1 class="bigtitle"> Bienvenue sur le site Hello Mangas' World </h1>
                </div>
                <div class="col col-sm-1 col-md-1 col-xl-1 waves" style="background-image: url('{{ asset('/Images/backgroundnippon.png')}}');"></div>

            </div>
            @yield('content')
        </main>
    </div>



    @include('layouts.footer')