@extends('layouts.app')
@section('titre')
    produit
@endsection
@section('content')
    <h1>Les produits</h1>
    <div class="row-fluid add-bouton"> <a class="animated-link2" href="/admin/produits/create"><span>Ajouter</span></a></div>
    <table class="table table-responsive table-striped">
        <thead>
            <th>Id</th>
            <th>Nom</th>
            <th>Image</th>
            <th>Genre</th>
            <th>Modifier</th>
            <th>Consulter</th>
            <th>Supprimer</th>
        </thead>
        <tbody>
            @foreach ($lesProduits as $produit)
                <tr>
                    <td>{{ $produit->id }}</td>
                    <td>{{ $produit->nom }}</td>
                    <td><img src="{{ asset('/Images/'. ($produit->image ?? 'produits/default.jpg')) }}" class="img-fluid" style="height:200px; width:200px;" alt=""></td>
                    <td>{{ $produit->categorie->genre }}</td>
                    <td><a class="animated-link" href="/admin/produits/{{ $produit->id }}/edit"><span>Modifier</span></a></td>
                    <td><a class="animated-link" href="/admin/produits/{{ $produit->id }}"><span>Consulter</span></a></td>
                       <td> <form class="supform" action="/admin/produits/{{ $produit->id }}" method="post">
                            @method('delete')
                            @csrf
                            <button class="animated-link"><span> Supprimer </span></button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection
