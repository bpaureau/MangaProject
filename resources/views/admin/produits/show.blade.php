@extends('layouts.app')
@section('titre')
    {{ $produit->nom }}
@endsection
@section('content')
    <h1> Informations au sujet de {{ Str::ucfirst($produit->nom) }} :</h1>
   <div class="container container-fluid d-flex justify-content-center align-items-center container-produit-read">
        <div class="card mb-5 mt-5" style="border: solid 4px; border-color: #CE122C; ">
            <div class="row g-0">
              <div class="col-md-4 ml-2 d-flex justify-content-center align-items-center">
                <img src="{{ asset('/Images/'. ($produit->image ?? 'produits/default.jpg')) }}" class="img-fluid rounded-start" alt=" {{ $produit->image}} ">
              </div>
              <div class="col-md-8">
                <div class="card-body">
                  <h5 class="card-title"> Genre : {{ $produit->categorie->genre }} </h5>
                  <p class="card-text" style="font-size: 25px; font-weight:bold"> Description : {{ $produit->description }} </p>

                </div>
              </div>
            </div>
          </div>
         </div>


@endsection
