@extends('layouts.app')
@section('titre')
    Categories
@endsection
@section('content')
<div class="container-fluid containerCategories">
    <h1>Les categories</h1>
    <div class="row-fluid add-bouton"> <a class="animated-link2" href="/admin/categories/create"><span>Ajouter</span></a></div>
    <table class="table table-responsive table-striped">
        <thead>
            <th></th>
            <th>Id</th>
            <th>Nom</th>
            <th>Modifier</th>
            <th>Consulter</th>
            <th>Supprimer</th>
        </thead>
        <tbody>
            @foreach ($lesCategories as $categorie)
                <tr>
                    <td>#</td>
                    <td>{{ $categorie->id }}</td>
                    <td>{{ $categorie->genre }}</td>
                    <td><a class="animated-link" href="/admin/categories/{{ $categorie->id }}/edit"><span>Modifier</span></a></td>
                    <td><a class="animated-link" href="/admin/categories/{{ $categorie->id }}"><span>Consulter</span></a></td>
                       <td> <form class="supform" action="/admin/categories/{{ $categorie->id }}" method="post">
                            @method('delete')
                            @csrf
                            <button class="animated-link"><span>Supprimer</span></button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection

@section('scriptes')
    <script>
        let lesFormulaires = document.querySelectorAll(".supform");

        for (const unFormulaire of lesFormulaires) {
            unFormulaire.addEventListener("submit", function(event) {

                if (confirm("Supprimer la categorie ?") == false) {
                    event.preventDefault();
                    return false;
                }
            })
        }
    </script>
@endsection
