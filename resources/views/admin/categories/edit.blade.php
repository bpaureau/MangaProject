@extends('layouts.app')
@section('titre')
    Modifier une categorie
@endsection
@section('content')
<div class="row-fluid">
    <div class="container">
        <div class="col-12 col-sm-10 col-md-6 col-lg-6 mx-auto">
            <h1 class="my-1">Modifier une categorie</h1>
            <form action="/admin/categories/{{ $categorie->id }}" method="post">
                @csrf  {{-- Protection incorporée à Laravel contre les Cross-Site Request Forgery --}}
                @method('put') {{-- Méthode PUT pour modifier l'existant --}}
                <div class='row mt-3 mb-2'>
                    <label for='nom'class="police">Nom *</label>
                    <input value='{{ old('nom') ?? $categorie->nom }}' name='nom' required type='text'
                        class="form-control mb-3" id="nom" placeholder="Saisir un nom">
                    @error('nom')
                        <div class='alert alert-danger mt-1'>{{ $message }}</div>
                    @enderror
                </div>

                <button type="submit" class="animated-link"><span>Valider</span></button>
            </form>
        </div>
    </div>
</div>
@endsection
