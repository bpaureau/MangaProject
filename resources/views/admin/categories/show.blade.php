@extends('layouts.app')
@section('titre')
    {{ $categorie->nom }}
@endsection
@section('content')
    <h1>Mangas compris dans la catégorie {{ Str::ucfirst($categorie->genre) }} :</h1>
    <table class="table table-responsive table-striped table-categorie-content">
        <thead>
            <th></th>
        </thead>
        <tbody>
            @foreach ($categorie->produits as $unProduit)
            <tr>
                <td> {{ $unProduit->nom }} </td>
            </tr>
            @endforeach
        </tbody>
        </table>


@endsection
