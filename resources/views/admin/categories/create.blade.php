@extends('layouts.app')
@section('titre')
    Ajout Categorie
@endsection
@section('content')
    <div class="container ajout-categorie-container">
        <div class="col-12 col-sm-10 col-md-6 col-lg-6 mx-auto">
            <h1 class="my-1">Ajout d'une categorie</h1>
            <form action="/admin/categories" method="post">
                @csrf

                <div class='row mt-3 mb-2'>
                    <label for='genre'class="police">Genre *</label>
                    <input value='{{ old('genre') }}' name='genre' required type='text' class="form-control mb-3" id="genre"
                        placeholder="Saisir un genre">
                    @error('genre')
                        <div class='alert alert-danger mt-1'>{{ $message }}</div>
                    @enderror
                </div>

                <button type="submit" class="animated-link"><span>Submit</span></button>
            </form>
        </div>
    </div>
@endsection
