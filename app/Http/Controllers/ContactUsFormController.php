<?php

namespace App\Http\Controllers;

use App\Mail\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactUsFormController extends Controller
{
    public function AfficheForm(){

        return view("client.formulaire");
    }

    public function traitementForm(Request $request){

       $attributs = $request->validate([

        "email" => "required|email",
        "sujet" => "required|string|min:2",
        "texte" => "required|string|min:2|max:300",
        "captcha" => "required|captcha"
       ]);

       //Envoi du Mail
       Mail::to('bpaureau@gmail.com')->send(new Contact($attributs["email"], $attributs["sujet"], $attributs["texte"]));
       session()->flash("success", "Email envoyé");
       return redirect("/contact");

    }
    public function reloadCaptcha()
    {
        return response()->json(['captcha'=> captcha_img()]);
    }
}
