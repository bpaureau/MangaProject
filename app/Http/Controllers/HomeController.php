<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $utilisateur=auth()-> user();
        if($utilisateur != null && $utilisateur->role->nom == "admin"){

            return view('admin.landingPageAdmin');
        }else if($utilisateur != null && $utilisateur->role->nom == "client"){

            return view('client.landingPage');

        }else{

            return view("auth.login");
        }

    }
}
