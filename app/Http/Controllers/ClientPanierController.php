<?php

namespace App\Http\Controllers;

use App\Models\Livre;
use App\Models\Produit;
use Darryldecode\Cart\Facades\CartFacade;
use Illuminate\Http\Request;

class ClientPanierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $panier= CartFacade::getContent();
       return view("client.panier.index", ["panier" => $panier]);
    }




    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Produit  $produit
     * @return \Illuminate\Http\Response
     */
    public function show(Produit $produit)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Produit  $produit
     * @return \Illuminate\Http\Response
     */
    public function edit(Livre $livre)
    {
        $ligne = CartFacade::get($livre->id);
        return view("client.edit", ["ligne" => $ligne ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Produit  $produit
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, Livre $livre)
    {

        $request->validate(["quantite"=> "required|numeric|min:1"]);
        if(CartFacade::get($livre->id)){

            CartFacade::update(
                $request->quantite,
                array(
                'id' => $livre->id,
                'name' => $livre->nom,
                'price' => $livre->prix,
                'quantity' => array(
                     'relative' => false,
                     'value' => $request->quantite
            ),

                'attributes' => array(),
            ));

         }
         else{
            CartFacade::add(array(
                'id' => $livre->id,
                'name' => $livre->nom,
                'price' => $livre->prix,
                // 'quantity' => array(
                //      'relative' => false,
                //      'value' => $request->input("quantite")
                'quantity'=> $request->quantite,
                'attributes' => array()
            ));
        }

        session()->flash("success", "Le produit est ajouté au panier");
        return redirect("/catalogue");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Produit  $produit
     * @return \Illuminate\Http\Response
     */
    public function destroy(Livre $livre)
    {
        CartFacade::remove($livre->id);
        session()->flash("success", "Le produit est retiré du panier");
        return redirect("/panier");

    }

    public function vider(){

        CartFacade::clear();
        session()->flash("success", "Le panier est vide");
        return redirect("/catalogue");
    }
}
