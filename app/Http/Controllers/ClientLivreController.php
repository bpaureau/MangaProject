<?php

namespace App\Http\Controllers;

use App\Models\Livre;
use App\Models\Produit;
use Illuminate\Http\Request;

class ClientLivreController extends Controller
{
    public function index()
    {
        // ici on affiche si il n'y a pas de recherche l'ensemble des livres triés par ordre alphabétique, sinon par recherche.
        $lesLivres = Livre::recherche()->orderBy("nom")->paginate(15);
        return view("client.catalogue", ["lesLivres" => $lesLivres]);
    }

    public function show(Livre $livre)
    {
        return view("client.show", ["unLivre" => $livre]);
    }
}
