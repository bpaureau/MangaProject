<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class OnlyAdmin
{
 
    public function handle(Request $request, Closure $next)
    {
        $utilisateur=auth()-> user();
        if($utilisateur != null && $utilisateur->role->nom == "admin"){

            return $next($request);
        }else{

            abort("404");
        }
    }
}
