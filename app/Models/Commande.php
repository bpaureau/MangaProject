<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Commande extends Model
{
    use HasFactory;


    public function user(){

        return $this->belongsTo(User::class, "user_id");

    }

    public function lignes(){

        return $this->belongsToMany(Livre::class, "lignes")->using(Ligne::class)->withPivot(["quantite"])->withTimestamps();
    }

    public function calcTotal(){
        $resultat=0;
        foreach($this->produitCommande as $ligne){
            $resultat=$ligne->prixUnitaire*$ligne->pivot->quantite;
        }
        return $resultat;
    }
}


