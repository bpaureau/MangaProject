<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Livre extends Model
{
    use HasFactory;
    protected $fillable = ["nom","description","prix","categorie_id","image"];

    public function livre(){
        return $this->belongsTo(Produit::class, "produit_id");
    }

    public function lignes(){

        return $this->belongsToMany(Commande::class, "lignes")->using(Ligne::class)->withPivot(["quantite"])->withTimestamps();
    }

    public function scopeRecherche($query){
        //Verifie si le nom d'un livre est transmis en get
        if(request("nom")){

            $query->where("nom","like","%".request("nom")."%");
        }
        //Si ce n'est pas le cas, cela correspond a select *
    }

}


