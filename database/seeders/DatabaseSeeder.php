<?php

namespace Database\Seeders;

use App\Models\Categorie;
use App\Models\Commande;
use App\Models\Ligne;
use App\Models\Livre;
use App\Models\Produit;
use App\Models\Role;
use App\Models\User;
use CreateLivresTable;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

       $roleAdmin = Role::create([
            "nom"=>"admin"
       ]);

       $roleClient = Role::create([
            "nom"=>"client"
       ]);

        $moi = User::create([
          "name"=>"Baptiste",
          "email"=>"bpaureau@gmail.com",
          "password"=>bcrypt("alicia"),
          "role_id"=>"$roleAdmin->id",
          "date_naissance"=>"1989-09-14",
          "ville"=>"Chevilly-Larue",
          "codepostal"=>"94550",
        ]);

        $client = User::create([

          "name"=>"Chantal",
          "email"=>"chantal.khim@live.fr",
          "password"=>bcrypt("tibo"),
          "role_id"=>"$roleClient->id",
          "date_naissance"=>"1990-11-08",


        ]);
        $categorie1 = Categorie::create(["genre"=>"Shōnen"]);
        $categorie2 = Categorie::create(["genre"=>"Shojo"]);
        $categorie3 = Categorie::create(["genre"=>"Seinen"]);
        $categorie4 = Categorie::create(["genre"=>"Manhwa"]);

/* Categorie Shonen */

        Produit::create([
            "nom"=>"My Hero Academia",
            "image"=>"myheroacademiamanga.jpg",
            "description"=> "Izuku Midoriya, fanboy de super-héros, est né sans don. Après une rencontre fortuite avec le plus grand super-héros du monde, All Might, il jure de travailler aussi dur que possible pour devenir un symbole de paix et une lueur d'espoir pour le monde.",
            "categorie_id"=> 1
        ]);

        Produit::create([
            "nom"=>"Bleach",
            "image"=>"bleachmanga.jpg",
            "description"=> "Adolescent de quinze ans, Ichigo Kurosaki possède un don particulier : celui de voir les esprits. Un jour, il croise la route d'une belle shinigami (un être spirituel) en train de pourchasser une “âme perdue”, un esprit maléfique qui hante notre monde et n'arrive pas à trouver le repos.",
            "categorie_id"=> 1
        ]);

        Produit::create([
            "nom"=>"Demon Slayer",
            "image"=>"demonslayermanga.jpg",
            "description"=> "Le groupe de Tanjirô a terminé son entraînement de récupération au domaine des papillons et embarque à présent en vue de sa prochaine mission à bord du train de l'infini, d'où quarante personnes ont disparu en peu de temps. Tanjirô et Nezuko, accompagnés de Zen'itsu et Inosuke, s'allient à l'un des plus puissants épéistes de l'armée des pourfendeurs de démons, le Pilier de la Flamme Kyôjurô Rengoku, afin de contrer le démon qui a engagé le train de l'Infini sur une voie funeste.",
            "categorie_id"=> 1
        ]);

        Produit::create([
            "nom"=>"Shingeki No Kyojin",
            "image"=>"shingekinokyojinmanga.jpg",
            "description"=> "Pendant 100 ans les humains ont connu la paix. Eren est un jeune garçon qui rêve de sortir de la ville pour explorer le monde extérieur. Il mène une vie paisible avec ses parents et sa sœur Mikasa dans le district de Shiganshina. Mais un jour de l'année 845, un Titan de plus de 60 mètres de haut apparaît.",
            "categorie_id"=> 1
        ]);
        Produit::create([
            "nom"=>"One Piece",
            "image"=>"onepiecemanga.jpg",
            "description"=> "Monkey D. Luffy rêve de retrouver ce trésor légendaire et de devenir le nouveau 'Roi des Pirates'. Après avoir mangé un fruit du démon, il possède un pouvoir lui permettant de réaliser son rêve. Il lui faut maintenant trouver un équipage pour partir à l'aventure !",
            "categorie_id"=> 1
        ]);

        /* Shojo */

        Produit::create([
            "nom"=>"Ijiranaide, Nagatoro-san",
            "image"=>"ijiranaidenagatorosanmanga.jpg",
            "description"=> "Nagatoro, une jeune lycéenne de première année au caractère moqueur voir légèrement cruel, fait un jour la rencontre d'un lycéen timide et solitaire. Bien que celui-ci soit son Senpai, la jeune fille passe son temps à se moquer de lui et à le martyriser.",
            "categorie_id"=> 2
        ]);

        Produit::create([
            "nom"=>"Komi-san wa, Commu-shou Desu",
            "image"=>"komisanwacomyushoudesumanga.jpg",
            "description"=> "Shôko Komi est une adolescente à la beauté atypique que ses camarades et professeurs placent volontiers sur un piédestal. Ainsi, dès son premier jour au lycée, une barrière invisible s'est créée entre elle et le reste de sa classe, qui la perçoit comme inaccessible.",
            "categorie_id"=> 2
        ]);

        Produit::create([
            "nom"=>"Clannad",
            "image"=>"clannadmanga.jpg",
            "description"=> "Il commence à rêver d'un monde sombre dans lequel vit une jeune fille ayant la capacité de créer des objets uniquement à partir d'ordures. Tomoya entre ainsi dans ce monde vide et sombre et suit la fille, la seule autre 'survivante' dans ce monde.",
            "categorie_id"=> 2
        ]);

        Produit::create([
            "nom"=>"Horimiya",
            "image"=>"horimiyamanga.jpg",
            "description"=> "Populaire et admirée par tous les garçons, Kyôko Hori, surnommé Hori, semble être une jeune fille ordinaire. Mais la vrai Hori, en dehors du lycée, est loin de là. Comme ses parents, obsédés par le travail, ne viennent à la maison que rarement, c'est elle qui s'occupe de tout pendant leur absence. Elle partage son temps entre devoirs scolaires, tâches ménagères et son petit frère. Elle n'a donc pas de temps pour une vie sociale d'adolescente.",
            "categorie_id"=> 2
        ]);
        Produit::create([
            "nom"=>"Fruit Basket",
            "image"=>"fruitbasketmanga.jpg",
            "description"=> "Tohru Honda, jeune orpheline de seize ans ayant perdu sa mère dans un tragique accident de voiture, vit seule dans une tente sur un terrain vague. C'était sans compter sur la générosité du propriétaire du terrain, Shigure Soma, qui lui propose de l'héberger le temps qu'elle retrouve un logement décent.",
            "categorie_id"=> 2
        ]);

        /* Seinen */

        Produit::create([
            "nom"=>"Berserk",
            "image"=>"berserkmanga.jpg",
            "description"=> "Dans un monde médiéval et fantastique, erre un guerrier solitaire nommé Guts, décidé à être seul maître de son destin. Autrefois contraint par un pari perdu à rejoindre les Faucons, une troupe de mercenaires dirigés par Griffith, Guts fut acteur de nombreux combats sanglants et témoin de sombres intrigues politiques.",
            "categorie_id"=> 3
        ]);

        Produit::create([
            "nom"=>"Akira",
            "image"=>"akiramanga.jpg",
            "description"=> "Tetsuo, un adolescent ayant vécu une enfance difficile, est la victime d'expériences visant à développer les capacités psychiques qui dorment en chacun de nous. Ainsi doté d'une puissance que lui meme ne peut imaginer, Tetsuo décide de partir en guerre contre le monde qui l'a opprimé. Dès lors, Il se retrouve au coeur d'une légende populaire qui annonce le retour prochain d'Akira, un enfant aux pouvoirs extra-ordinaires censé délivrer Tokyo du chaos...",
            "categorie_id"=> 3
        ]);

        Produit::create([
            "nom"=>"Monster",
            "image"=>"monstermanga.jpg",
            "description"=> "Kenzo Tenma est un médecin japonais travaillant en Allemagne. Il décide de sauver la vie d'un jeune garçon au lieu du maire, estimant que chaque vie a la même valeur. Quelques jours plus tard, le directeur et deux autres responsables de l'hôpital sont empoisonnés.",
            "categorie_id"=> 3
        ]);

        Produit::create([
            "nom"=>"Gunnm",
            "image"=>"Gunnmmanga.jpg",
            "description"=> "Gally est un cyborg créé par le docteur Ido qui est aussi un chasseur de prime.
            Ce dernier la trouve alors qu'il cherchait des pièces pour cyborgs dans une décharge.
            Gally suit ensuite les traces de son créateur Ido et devient chasseur de prime.",
            "categorie_id"=> 3
        ]);

        Produit::create([
            "nom"=>"Pluto",
            "image"=>"plutomanga.jpg",
            "description"=> "Mont-Blanc, un célèbre robot qui a combattu lors de la dernière guerre asiatique, a été assassiné. Cette mort plonge le monde de le deuil.
            L'inspecteur Gesicht, robot fatigué et déprimé, ne tarde pas à découvrir une piste, mais l'enquête s'avère difficile faute d'indices et l'adversaire, un autre robot, trop puissant... Gesicht décide de partir en voyage afin de rencontrer d'anciens camarades de guerre de Mont-Blanc susceptibles d'être à leur tour attaqués par l'ennemi.
            Mais Pluto, le robot tueur, ne perd pas de temps...",
            "categorie_id"=> 3
        ]);


        /* Debut Produits Manhwa */


        Produit::create([
            "nom"=>"Solo Leveling",
            "image"=>"sololevelingmanga.jpg",
            "description"=> "Lorsque d'étranges portails sont apparus aux quatre coins du monde, l'humanité a dû trouver une parade pour ne pas finir massacrée entre les griffes des monstres qu'ils ont apportés avec eux. Dans le même temps, certaines personnes ont développé des capacités permettant de chasser.",
            "categorie_id"=> 4
        ]);

        Produit::create([
            "nom"=>"Tower of God",
            "image"=>"towerofgodmanga.jpg",
            "description"=> "Afin de retrouver Rachel, la seule personne chère à ses yeux, Bam décide de prendre tous les risques pour atteindre le sommet d'une mystérieuse tour. Pour passer chaque étage, il devra réussir un test complexe dans lequel il jouera à chaque fois sa vie.",
            "categorie_id"=> 4
        ]);

        Produit::create([
            "nom"=>"TAL",
            "image"=>"talmanga.jpg",
            "description"=> "Yu jin est un lycéen ordinaire qui vient de perdre sa grand-mère. Pourtant, il va être choisi comme successeur au Roi des Chachaoongs.
            Les Chachaoongs ont l'air à première vue tout à fait normal, cependant ils n'ont aucun souvenir, ils ne savent pas d'où ils viennent ni comment ils sont nés, ils possèdent également une étrange capacité ; celle de matérialiser une image qu'ils ont en tête.
            Les Chachaoongs vont-ils supporter qu'un humain les gouvernes ?",
            "categorie_id"=> 4
        ]);

        Produit::create([
            "nom"=>"Overgeared",
            "image"=>"overgearedmanga.jpg",
            "description"=> "Youngwoo n'est pas très doué, il n'a aucun talent, n'est ni particulièrement beau ni intelligent. Il a arrêté ses études pour jouer à Satisfy, le plus grand VMMORPG de tous les temps, avec plus de 2 milliards d'utilisateurs, et enchaîne les petits boulots pour pouvoir se payer ce qu'il veut dans le jeu.",
            "categorie_id"=> 4
        ]);

        Produit::create([
            "nom"=>"Dalsez",
            "image"=>"dalsezmanga.jpg",
            "description"=> "Savez-vousce qu'il y a derrière les abysses? Riend dîtes vous... pourtant ce n'est peut-être pas le cas. Personne encore n'est allé vérifier.... ces créatures aux yeux d'or... quelles sont-elles?
            D'ou viennent-elles? Que nous veulent-elles?",
            "categorie_id"=> 4
        ]);
        /*MHA*/

        $livre1 = Livre::create([
            "stock"=> 5,
            "nom"=> "My Hero Academia Tome 01",
            "imageURL"=> "mhat01.jpg",
            "prix" => "6.60"

        ]);

        Livre::create([
            "stock"=> 10,
            "nom"=> "My Hero Academia Tome 02",
            "imageURL"=> "mhat02.jpg",
            "prix" => "6.60"
        ]);


        Livre::create([
            "stock"=> 7,
            "nom"=> "My Hero Academia Tome 03",
            "imageURL"=> "mhat03.jpg",
            "prix" => "6.60"

        ]);


        Livre::create([
            "stock"=> 5,
            "nom"=> "My Hero Academia Tome 04",
            "imageURL"=> "mhat04.jpg",
            "prix" => "6.60"

        ]);


        Livre::create([
            "stock"=> 8,
            "nom"=> "My Hero Academia Tome 05",
            "imageURL"=> "mhat05.jpg",
            "prix" => "6.60"

        ]);


        Livre::create([
            "stock"=> 9,
            "nom"=> "My Hero Academia Tome 06",
            "imageURL"=> "mhat06.jpg",
            "prix" => "6.60"

        ]);


        Livre::create([
            "stock"=> 3,
            "nom"=> "My Hero Academia Tome 07",
            "imageURL"=> "mhat07.jpg",
            "prix" => "6.60"

        ]);


        Livre::create([
            "stock"=> 5,
            "nom"=> "My Hero Academia Tome 08",
            "imageURL"=> "mhat08.jpg",
            "prix" => "6.60"

        ]);


        Livre::create([
            "stock"=> 5,
            "nom"=> "My Hero Academia Tome 09",
            "imageURL"=> "mhat09.jpg",
            "prix" => "6.60"

        ]);

        Livre::create([
            "stock"=> 10,
            "nom"=> "My Hero Academia Tome 10",
            "imageURL"=> "mhat10.jpg",
            "prix" => "6.60"

        ]);

        /*BLEACH*/

        Livre::create([
            "stock"=> 4,
            "nom"=> "Bleach Tome 01",
            "imageURL"=> "bleacht01.jpg",
            "prix" => "6.90"

        ]);

        Livre::create([
            "stock"=> 13,
            "nom"=> "Bleach Tome 02",
            "imageURL"=> "bleacht02.jpg",
            "prix" => "6.90"

        ]);

        Livre::create([
            "stock"=> 6,
            "nom"=> "Bleach Tome 03",
            "imageURL"=> "bleacht03.jpg",
            "prix" => "6.90"

        ]);

        Livre::create([
            "stock"=> 8,
            "nom"=> "Bleach Tome 04",
            "imageURL"=> "bleacht04.jpg",
            "prix" => "6.90"

        ]);

        Livre::create([
            "stock"=> 10,
            "nom"=> "Bleach Tome 05",
            "imageURL"=> "bleacht05.jpg",
            "prix" => "6.90"

        ]);

        Livre::create([
            "stock"=> 9,
            "nom"=> "Bleach Tome 06",
            "imageURL"=> "bleacht06.jpg",
            "prix" => "6.90"

        ]);

        Livre::create([
            "stock"=> 1,
            "nom"=> "Bleach Tome 07",
            "imageURL"=> "bleacht07.jpg",
            "prix" => "6.90"

        ]);

        Livre::create([
            "stock"=> 8,
            "nom"=> "Bleach Tome 08",
            "imageURL"=> "bleacht08.jpg",
            "prix" => "6.90"

        ]);

        Livre::create([
            "stock"=> 5,
            "nom"=> "Bleach Tome 09",
            "imageURL"=> "bleacht09.jpg",
            "prix" => "6.90"

        ]);

        Livre::create([
            "stock"=> 3,
            "nom"=> "Bleach Tome 10",
            "imageURL"=> "bleacht10.jpg",
            "prix" => "6.90"

        ]);


        /*DEMON SLAYER*/

        Livre::create([
            "stock"=> 4,
            "nom"=> "Demon Slayer Tome 01",
            "imageURL"=> "demonslayert01.jpg",
            "prix" => "7.29"

        ]);

        Livre::create([
            "stock"=> 6,
            "nom"=> "Demon Slayer Tome 02",
            "imageURL"=> "demonslayert02.jpg",
            "prix" => "7.29"

        ]);

        Livre::create([
            "stock"=> 7,
            "nom"=> "Demon Slayer Tome 03",
            "imageURL"=> "demonslayert03.jpg",
            "prix" => "7.29"

        ]);

        Livre::create([
            "stock"=> 12,
            "nom"=> "Demon Slayer Tome 04",
            "imageURL"=> "demonslayert04.jpg",
            "prix" => "7.29"

        ]);

        Livre::create([
            "stock"=> 11,
            "nom"=> "Demon Slayer Tome 05",
            "imageURL"=> "demonslayert05.jpg",
            "prix" => "7.29"

        ]);

        Livre::create([
            "stock"=> 9,
            "nom"=> "Demon Slayer Tome 06",
            "imageURL"=> "demonslayert06.jpg",
            "prix" => "7.29"

        ]);

        Livre::create([
            "stock"=> 20,
            "nom"=> "Demon Slayer Tome 07",
            "imageURL"=> "demonslayert07.jpg",
            "prix" => "7.29"

        ]);

        Livre::create([
            "stock"=> 3,
            "nom"=> "Demon Slayer Tome 08",
            "imageURL"=> "demonslayert08.jpg",
            "prix" => "7.29"

        ]);

        Livre::create([
            "stock"=> 10,
            "nom"=> "Demon Slayer Tome 09",
            "imageURL"=> "demonslayert09.jpg",
            "prix" => "7.29"

        ]);

        Livre::create([
            "stock"=> 4,
            "nom"=> "Demon Slayer Tome 10",
            "imageURL"=> "demonslayert10.jpg",
            "prix" => "7.29"

        ]);

        /* Shingeki No Kyojin */

        Livre::create([
            "stock"=> 5,
            "nom"=> "Shingeki No Kyojin Tome 01",
            "imageURL"=> "shingekinokyojint01.jpg",
            "prix" => "11.14"

        ]);

        Livre::create([
            "stock"=> 5,
            "nom"=> "Shingeki No Kyojin Tome 02",
            "imageURL"=> "shingekinokyojint02.jpg",
            "prix" => "11.14"

        ]);

        Livre::create([
            "stock"=> 6,
            "nom"=> "Shingeki No Kyojin Tome 03",
            "imageURL"=> "shingekinokyojint03.jpg",
            "prix" => "11.14"

        ]);

        Livre::create([
            "stock"=> 8,
            "nom"=> "Shingeki No Kyojin Tome 04",
            "imageURL"=> "shingekinokyojint04.jpg",
            "prix" => "11.14"

        ]);

        Livre::create([
            "stock"=> 9,
            "nom"=> "Shingeki No Kyojin Tome 05",
            "imageURL"=> "shingekinokyojint05.jpg",
            "prix" => "11.14"

        ]);

        Livre::create([
            "stock"=> 10,
            "nom"=> "Shingeki No Kyojin Tome 06",
            "imageURL"=> "shingekinokyojint06.jpg",
            "prix" => "11.14"

        ]);

        Livre::create([
            "stock"=> 11,
            "nom"=> "Shingeki No Kyojin Tome 07",
            "imageURL"=> "shingekinokyojint07.jpg",
            "prix" => "11.14"

        ]);

        Livre::create([
            "stock"=> 4,
            "nom"=> "Shingeki No Kyojin Tome 08",
            "imageURL"=> "shingekinokyojint08.jpg",
            "prix" => "11.14"

        ]);

        Livre::create([
            "stock"=> 5,
            "nom"=> "Shingeki No Kyojin Tome 09",
            "imageURL"=> "shingekinokyojint09.jpg",
            "prix" => "11.14"

        ]);

        Livre::create([
            "stock"=> 5,
            "nom"=> "Shingeki No Kyojin Tome 10",
            "imageURL"=> "shingekinokyojint10.jpg",
            "prix" => "11.14"

        ]);


        /*One Piece*/

        Livre::create([
            "stock"=> 4,
            "nom"=> "One Piece Tome 01",
            "imageURL"=> "onepiecet01.jpg",
            "prix" => "6.90"

        ]);

        Livre::create([
            "stock"=> 6,
            "nom"=> "One Piece Tome 02",
            "imageURL"=> "onepiecet02.jpg",
            "prix" => "6.90"

        ]);

        Livre::create([
            "stock"=> 10,
            "nom"=> "One Piece Tome 03",
            "imageURL"=> "onepiecet03.jpg",
            "prix" => "6.90"

        ]);

        Livre::create([
            "stock"=> 13,
            "nom"=> "One Piece Tome 04",
            "imageURL"=> "onepiecet04.jpg",
            "prix" => "6.90"

        ]);

        Livre::create([
            "stock"=> 2,
            "nom"=> "One Piece Tome 05",
            "imageURL"=> "onepiecet05.jpg",
            "prix" => "6.90"

        ]);

        Livre::create([
            "stock"=> 9,
            "nom"=> "One Piece Tome 06",
            "imageURL"=> "onepiecet06.jpg",
            "prix" => "6.90"

        ]);

        Livre::create([
            "stock"=> 7,
            "nom"=> "One Piece Tome 07",
            "imageURL"=> "onepiecet07.jpg",
            "prix" => "6.90"

        ]);

        Livre::create([
            "stock"=> 12,
            "nom"=> "One Piece Tome 08",
            "imageURL"=> "onepiecet08.jpg",
            "prix" => "6.90"

        ]);

        Livre::create([
            "stock"=> 9,
            "nom"=> "One Piece Tome 09",
            "imageURL"=> "onepiecet09.jpg",
            "prix" => "6.90"

        ]);

        Livre::create([
            "stock"=> 8,
            "nom"=> "One Piece Tome 10",
            "imageURL"=> "onepiecet10.jpg",
            "prix" => "6.90"

        ]);

        /* Début des SHOJO */

        /* Ijiranaide Nagatoro-san*/

        Livre::create([
            "stock"=> 6,
            "nom"=> "Ijiranaide Nagatoro-san Tome 01",
            "imageURL"=> "ijiranaidenagatorosant01.jpg",
            "prix" => "7.95"

        ]);

        Livre::create([
            "stock"=> 4,
            "nom"=> "Ijiranaide Nagatoro-san Tome 02",
            "imageURL"=> "ijiranaidenagatorosant02.jpg",
            "prix" => "7.95"

        ]);

        Livre::create([
            "stock"=> 7,
            "nom"=> "Ijiranaide Nagatoro-san Tome 03",
            "imageURL"=> "ijiranaidenagatorosant03.jpg",
            "prix" => "7.95"

        ]);

        Livre::create([
            "stock"=> 2,
            "nom"=> "Ijiranaide Nagatoro-san Tome 04",
            "imageURL"=> "ijiranaidenagatorosant04.jpg",
            "prix" => "7.95"

        ]);

        Livre::create([
            "stock"=> 4,
            "nom"=> "Ijiranaide Nagatoro-san Tome 05",
            "imageURL"=> "ijiranaidenagatorosant05.jpg",
            "prix" => "7.95"

        ]);

        Livre::create([
            "stock"=> 6,
            "nom"=> "Ijiranaide Nagatoro-san Tome 06",
            "imageURL"=> "ijiranaidenagatorosant06.jpg",
            "prix" => "7.95"

        ]);

        Livre::create([
            "stock"=> 12,
            "nom"=> "Ijiranaide Nagatoro-san Tome 07",
            "imageURL"=> "ijiranaidenagatorosant07.jpg",
            "prix" => "7.95"

        ]);

        Livre::create([
            "stock"=> 9,
            "nom"=> "Ijiranaide Nagatoro-san Tome 08",
            "imageURL"=> "ijiranaidenagatorosant08.jpg",
            "prix" => "7.95"

        ]);

        Livre::create([
            "stock"=> 7,
            "nom"=> "Ijiranaide Nagatoro-san Tome 09",
            "imageURL"=> "ijiranaidenagatorosant09.jpg",
            "prix" => "7.95"

        ]);
        Livre::create([
            "stock"=> 6,
            "nom"=> "Ijiranaide Nagatoro-san Tome 10",
            "imageURL"=> "ijiranaidenagatorosant10.jpg",
            "prix" => "7.95"

        ]);

        /* Komi-san wa, comyushou desu */

        Livre::create([
            "stock"=> 2,
            "nom"=> "Komi-san wa, comyushou desu Tome 01",
            "imageURL"=> "komisanwacomyushoudesut01.jpg",
            "prix" => "7.95"

        ]);

        Livre::create([
            "stock"=> 4,
            "nom"=> "Komi-san wa, comyushou desu Tome 02",
            "imageURL"=> "komisanwacomyushoudesut02.jpg",
            "prix" => "7.95"

        ]);

        Livre::create([
            "stock"=> 11,
            "nom"=> "Komi-san wa, comyushou desu Tome 03",
            "imageURL"=> "komisanwacomyushoudesut03.jpg",
            "prix" => "7.95"

        ]);

        Livre::create([
            "stock"=> 12,
            "nom"=> "Komi-san wa, comyushou desu Tome 04",
            "imageURL"=> "komisanwacomyushoudesut04.jpg",
            "prix" => "7.95"

        ]);

        Livre::create([
            "stock"=> 16,
            "nom"=> "Komi-san wa, comyushou desu Tome 05",
            "imageURL"=> "komisanwacomyushoudesut05.jpg",
            "prix" => "7.95"

        ]);

        Livre::create([
            "stock"=> 7,
            "nom"=> "Komi-san wa, comyushou desu Tome 06",
            "imageURL"=> "komisanwacomyushoudesut06.jpg",
            "prix" => "7.95"

        ]);

        Livre::create([
            "stock"=> 14,
            "nom"=> "Komi-san wa, comyushou desu Tome 07",
            "imageURL"=> "komisanwacomyushoudesut07.jpg",
            "prix" => "7.95"

        ]);

        Livre::create([
            "stock"=> 4,
            "nom"=> "Komi-san wa, comyushou desu Tome 08",
            "imageURL"=> "komisanwacomyushoudesut08.jpg",
            "prix" => "7.95"

        ]);

        Livre::create([
            "stock"=> 6,
            "nom"=> "Komi-san wa, comyushou desu Tome 09",
            "imageURL"=> "komisanwacomyushoudesut09.jpg",
            "prix" => "7.95"

        ]);

        Livre::create([
            "stock"=> 7,
            "nom"=> "Komi-san wa, comyushou desu Tome 10",
            "imageURL"=> "ijiranaidenagatorosant10.jpg",
            "prix" => "7.95"

        ]);

        /* Clannad */

        Livre::create([
            "stock"=> 9,
            "nom"=> "Clannad Tome 01",
            "imageURL"=> "clannadt01.jpg",
            "prix" => "7.99"

        ]);

        Livre::create([
            "stock"=> 6,
            "nom"=> "Clannad Tome 02",
            "imageURL"=> "clannadt02.jpg",
            "prix" => "7.99"

        ]);

        Livre::create([
            "stock"=> 12,
            "nom"=> "Clannad Tome 03",
            "imageURL"=> "clannadt03.jpg",
            "prix" => "7.99"

        ]);

        Livre::create([
            "stock"=> 9,
            "nom"=> "Clannad Tome 04",
            "imageURL"=> "clannadt04.jpg",
            "prix" => "7.99"

        ]);

        Livre::create([
            "stock"=> 2,
            "nom"=> "Clannad Tome 05",
            "imageURL"=> "clannadt05.jpg",
            "prix" => "7.99"

        ]);

        Livre::create([
            "stock"=> 3,
            "nom"=> "Clannad Tome 06",
            "imageURL"=> "clannadt06.jpg",
            "prix" => "7.99"

        ]);

        Livre::create([
            "stock"=> 18,
            "nom"=> "Clannad Tome 07",
            "imageURL"=> "clannadt07.jpg",
            "prix" => "7.99"

        ]);

        Livre::create([
            "stock"=> 4,
            "nom"=> "Clannad Tome 08",
            "imageURL"=> "clannadt08.jpg",
            "prix" => "7.99"

        ]);

        Livre::create([
            "stock"=> 6,
            "nom"=> "Clannad Tome 09",
            "imageURL"=> "clannadt09.jpg",
            "prix" => "7.99"

        ]);

        Livre::create([
            "stock"=> 6,
            "nom"=> "Clannad Tome 10",
            "imageURL"=> "clannadt10.jpg",
            "prix" => "7.99"

        ]);


        /* Horimiya */



        Livre::create([
            "stock"=> 6,
            "nom"=> "Horimiya Tome 01",
            "imageURL"=> "horimiyat01.jpg",
            "prix" => "7.20"

        ]);

        Livre::create([
            "stock"=> 4,
            "nom"=> "Horimiya Tome 02",
            "imageURL"=> "horimiyat02.jpg",
            "prix" => "7.20"

        ]);

        Livre::create([
            "stock"=> 11,
            "nom"=> "Horimiya Tome 03",
            "imageURL"=> "horimiyat03.jpg",
            "prix" => "7.20"

        ]);

        Livre::create([
            "stock"=> 9,
            "nom"=> "Horimiya Tome 04",
            "imageURL"=> "horimiyat04.jpg",
            "prix" => "7.20"

        ]);

        Livre::create([
            "stock"=> 16,
            "nom"=> "Horimiya Tome 05",
            "imageURL"=> "horimiyat05.jpg",
            "prix" => "7.20"

        ]);

        Livre::create([
            "stock"=> 6,
            "nom"=> "Horimiya Tome 06",
            "imageURL"=> "horimiyat06.jpg",
            "prix" => "7.20"

        ]);

        Livre::create([
            "stock"=> 6,
            "nom"=> "Horimiya Tome 07",
            "imageURL"=> "horimiyat07.jpg",
            "prix" => "7.20"

        ]);

        Livre::create([
            "stock"=> 5,
            "nom"=> "Horimiya Tome 08",
            "imageURL"=> "horimiyat08.jpg",
            "prix" => "7.20"

        ]);

        Livre::create([
            "stock"=> 5,
            "nom"=> "Horimiya Tome 09",
            "imageURL"=> "horimiyat09.jpg",
            "prix" => "7.20"

        ]);

        Livre::create([
            "stock"=> 4,
            "nom"=> "Horimiya Tome 10",
            "imageURL"=> "horimiyat10.jpg",
            "prix" => "7.20"

        ]);

        /* Fruit Basket */

        Livre::create([
            "stock"=> 4,
            "nom"=> "Fruit Basket Tome 01",
            "imageURL"=> "fruitbaskett01.jpg",
            "prix" => "12.50"

        ]);

        Livre::create([
            "stock"=> 5,
            "nom"=> "Fruit Basket Tome 02",
            "imageURL"=> "fruitbaskett02.jpg",
            "prix" => "12.50"

        ]);

        Livre::create([
            "stock"=> 14,
            "nom"=> "Fruit Basket Tome 03",
            "imageURL"=> "fruitbaskett03.jpg",
            "prix" => "12.50"

        ]);

        Livre::create([
            "stock"=> 2,
            "nom"=> "Fruit Basket Tome 04",
            "imageURL"=> "fruitbaskett04.jpg",
            "prix" => "12.50"

        ]);

        Livre::create([
            "stock"=> 5,
            "nom"=> "Fruit Basket Tome 05",
            "imageURL"=> "fruitbaskett05.jpg",
            "prix" => "12.50"

        ]);

        Livre::create([
            "stock"=> 3,
            "nom"=> "Fruit Basket Tome 06",
            "imageURL"=> "fruitbaskett06.jpg",
            "prix" => "12.50"

        ]);

        Livre::create([
            "stock"=> 7,
            "nom"=> "Fruit Basket Tome 07",
            "imageURL"=> "fruitbaskett07.jpg",
            "prix" => "12.50"

        ]);

        Livre::create([
            "stock"=> 4,
            "nom"=> "Fruit Basket Tome 08",
            "imageURL"=> "fruitbaskett08.jpg",
            "prix" => "12.50"

        ]);

        Livre::create([
            "stock"=> 13,
            "nom"=> "Fruit Basket Tome 09",
            "imageURL"=> "fruitbaskett09.jpg",
            "prix" => "12.50"

        ]);

        Livre::create([
            "stock"=> 6,
            "nom"=> "Fruit Basket Tome 10",
            "imageURL"=> "fruitbaskett10.jpg",
            "prix" => "12.50"

        ]);

        /* genre SEINEN*/

        /*Berserk*/

        Livre::create([
            "stock"=> 13,
            "nom"=> "Berserk Tome 01",
            "imageURL"=> "berserkt01.jpg",
            "prix" => "6.90"

        ]);


        Livre::create([
            "stock"=> 7,
            "nom"=> "Berserk Tome 02",
            "imageURL"=> "berserkt02.jpg",
            "prix" => "6.90"

        ]);

        Livre::create([
            "stock"=> 10,
            "nom"=> "Berserk Tome 03",
            "imageURL"=> "berserkt03.jpg",
            "prix" => "6.90"

        ]);

        Livre::create([
            "stock"=> 13,
            "nom"=> "Berserk Tome 04",
            "imageURL"=> "berserkt04.jpg",
            "prix" => "6.90"

        ]);

        Livre::create([
            "stock"=> 6,
            "nom"=> "Berserk Tome 05",
            "imageURL"=> "berserkt05.jpg",
            "prix" => "6.90"

        ]);

        Livre::create([
            "stock"=> 3,
            "nom"=> "Berserk Tome 06",
            "imageURL"=> "berserkt06.jpg",
            "prix" => "6.90"

        ]);

        Livre::create([
            "stock"=> 7,
            "nom"=> "Berserk Tome 07",
            "imageURL"=> "berserkt07.jpg",
            "prix" => "6.90"

        ]);

        Livre::create([
            "stock"=> 4,
            "nom"=> "Berserk Tome 08",
            "imageURL"=> "berserkt08.jpg",
            "prix" => "6.90"

        ]);

        Livre::create([
            "stock"=> 13,
            "nom"=> "Berserk Tome 09",
            "imageURL"=> "berserkt09.jpg",
            "prix" => "6.90"

        ]);

        Livre::create([
            "stock"=> 9,
            "nom"=> "Berserk Tome 10",
            "imageURL"=> "berserkt10.jpg",
            "prix" => "6.90"

        ]);

        /* Akira */

        Livre::create([
            "stock"=> 13,
            "nom"=> "Akira Tome 01",
            "imageURL"=> "akirat01.jpg",
            "prix" => "14.95"

        ]);


        Livre::create([
            "stock"=> 7,
            "nom"=> "Akira Tome 02",
            "imageURL"=> "akirat02.jpg",
            "prix" => "14.95"

        ]);

        Livre::create([
            "stock"=> 10,
            "nom"=> "Akira Tome 03",
            "imageURL"=> "akirat03.jpg",
            "prix" => "14.95"

        ]);

        Livre::create([
            "stock"=> 13,
            "nom"=> "Akira Tome 04",
            "imageURL"=> "akirat04.jpg",
            "prix" => "14.95"

        ]);

        Livre::create([
            "stock"=> 6,
            "nom"=> "Akira Tome 05",
            "imageURL"=> "akirat05.jpg",
            "prix" => "14.95"

        ]);

        Livre::create([
            "stock"=> 3,
            "nom"=> "Akira Tome 06",
            "imageURL"=> "akirat06.jpg",
            "prix" => "14.95"

        ]);

        Livre::create([
            "stock"=> 7,
            "nom"=> "Akira Tome 07",
            "imageURL"=> "akirat07.jpg",
            "prix" => "14.95"

        ]);

        Livre::create([
            "stock"=> 4,
            "nom"=> "Akira Tome 08",
            "imageURL"=> "akirat08.jpg",
            "prix" => "14.95"

        ]);

        Livre::create([
            "stock"=> 5,
            "nom"=> "Akira Tome 09",
            "imageURL"=> "akirat09.jpg",
            "prix" => "14.95"

        ]);

        Livre::create([
            "stock"=> 9,
            "nom"=> "Akira Tome 10",
            "imageURL"=> "akirat10.jpg",
            "prix" => "14.95"

        ]);

        /* Monster */

        Livre::create([
            "stock"=> 13,
            "nom"=> "Monster Tome 01",
            "imageURL"=> "monstert01.jpg",
            "prix" => "7.90"

        ]);


        Livre::create([
            "stock"=> 7,
            "nom"=> "Monster Tome 02",
            "imageURL"=> "monstert02.jpg",
            "prix" => "7.90"

        ]);

        Livre::create([
            "stock"=> 10,
            "nom"=> "Monster Tome 03",
            "imageURL"=> "monstert03.jpg",
            "prix" => "7.90"

        ]);

        Livre::create([
            "stock"=> 13,
            "nom"=> "Monster Tome 04",
            "imageURL"=> "monstert04.jpg",
            "prix" => "7.90"

        ]);

        Livre::create([
            "stock"=> 6,
            "nom"=> "Monster Tome 05",
            "imageURL"=> "monstert05.jpg",
            "prix" => "7.90"

        ]);

        Livre::create([
            "stock"=> 3,
            "nom"=> "Monster Tome 06",
            "imageURL"=> "monstert06.jpg",
            "prix" => "7.90"

        ]);

        Livre::create([
            "stock"=> 7,
            "nom"=> "Monster Tome 07",
            "imageURL"=> "monstert07.jpg",
            "prix" => "7.90"

        ]);

        Livre::create([
            "stock"=> 4,
            "nom"=> "Monster Tome 08",
            "imageURL"=> "monstert08.jpg",
            "prix" => "7.90"

        ]);

        Livre::create([
            "stock"=> 5,
            "nom"=> "Monster Tome 09",
            "imageURL"=> "monstert09.jpg",
            "prix" => "7.90"

        ]);



        /*gunnm */


        Livre::create([
            "stock"=> 13,
            "nom"=> "Gunnm Tome 01",
            "imageURL"=> "gunnmt01.jpg",
            "prix" => "7.90"

        ]);


        Livre::create([
            "stock"=> 7,
            "nom"=> "Gunnm Tome 02",
            "imageURL"=> "gunnmt02.jpg",
            "prix" => "7.90"

        ]);

        Livre::create([
            "stock"=> 10,
            "nom"=> "Gunnm Tome 03",
            "imageURL"=> "gunnmt03.jpg",
            "prix" => "7.90"

        ]);

        Livre::create([
            "stock"=> 13,
            "nom"=> "Gunnm Tome 04",
            "imageURL"=> "gunnmt04.jpg",
            "prix" => "7.90"

        ]);

        Livre::create([
            "stock"=> 6,
            "nom"=> "Gunnm Tome 05",
            "imageURL"=> "gunnmt05.jpg",
            "prix" => "7.90"

        ]);

        Livre::create([
            "stock"=> 3,
            "nom"=> "Gunnm Tome 06",
            "imageURL"=> "gunnmt06.jpg",
            "prix" => "7.90"

        ]);

        Livre::create([
            "stock"=> 7,
            "nom"=> "Gunnm Tome 07",
            "imageURL"=> "gunnmt07.jpg",
            "prix" => "7.90"

        ]);

        Livre::create([
            "stock"=> 4,
            "nom"=> "Gunnm Tome 08",
            "imageURL"=> "gunnmt08.jpg",
            "prix" => "7.90"

        ]);

        Livre::create([
            "stock"=> 5,
            "nom"=> "Gunnm Tome 09",
            "imageURL"=> "gunnmt09.jpg",
            "prix" => "7.90"

        ]);

        Livre::create([
            "stock"=> 9,
            "nom"=> "Gunnm Tome 10",
            "imageURL"=> "gunnmt10.jpg",
            "prix" => "7.90"

        ]);

        /*PLUTO */

        Livre::create([
            "stock"=> 13,
            "nom"=> "Pluto Tome 01",
            "imageURL"=> "plutot01.jpg",
            "prix" => "7.45"

        ]);


        Livre::create([
            "stock"=> 7,
            "nom"=> "Pluto Tome 02",
            "imageURL"=> "plutot02.jpg",
            "prix" => "7.45"

        ]);

        Livre::create([
            "stock"=> 10,
            "nom"=> "Pluto Tome 03",
            "imageURL"=> "plutot03.jpg",
            "prix" => "7.45"

        ]);

        Livre::create([
            "stock"=> 13,
            "nom"=> "Pluto Tome 04",
            "imageURL"=> "plutot04.jpg",
            "prix" => "7.45"

        ]);

        Livre::create([
            "stock"=> 6,
            "nom"=> "Pluto Tome 05",
            "imageURL"=> "plutot05.jpg",
            "prix" => "7.45"

        ]);

        Livre::create([
            "stock"=> 3,
            "nom"=> "Pluto Tome 06",
            "imageURL"=> "plutot06.jpg",
            "prix" => "7.45"

        ]);

        Livre::create([
            "stock"=> 7,
            "nom"=> "Pluto Tome 07",
            "imageURL"=> "plutot07.jpg",
            "prix" => "7.45"

        ]);

        Livre::create([
            "stock"=> 4,
            "nom"=> "Pluto Tome 08",
            "imageURL"=> "plutot08.jpg",
            "prix" => "7.45"

        ]);


        /* Genre MANHWA */
        /* Solo leveling*/


       $livre2 = Livre::create([
            "stock"=> 13,
            "nom"=> "Solo leveling Tome 01",
            "imageURL"=> "sololevelingt01.jpg",
            "prix" => "14.95"

        ]);


        Livre::create([
            "stock"=> 7,
            "nom"=> "Solo leveling Tome 02",
            "imageURL"=> "sololevelingt02.jpg",
            "prix" => "14.95"

        ]);

        Livre::create([
            "stock"=> 10,
            "nom"=> "Solo leveling Tome 03",
            "imageURL"=> "sololevelingt03.jpg",
            "prix" => "14.95"

        ]);

        Livre::create([
            "stock"=> 13,
            "nom"=> "Solo leveling Tome 04",
            "imageURL"=> "sololevelingt04.jpg",
            "prix" => "14.95"

        ]);

        Livre::create([
            "stock"=> 6,
            "nom"=> "Solo leveling Tome 05",
            "imageURL"=> "sololevelingt05.jpg",
            "prix" => "14.95"

        ]);

        Livre::create([
            "stock"=> 3,
            "nom"=> "Solo leveling Tome 06",
            "imageURL"=> "sololevelingt06.jpg",
            "prix" => "14.95"

        ]);

        Livre::create([
            "stock"=> 7,
            "nom"=> "Solo leveling Tome 07",
            "imageURL"=> "sololevelingt07.jpg",
            "prix" => "14.95"

        ]);




        /* Tower of God */


        Livre::create([
            "stock"=> 13,
            "nom"=> "Tower of God Tome 01",
            "imageURL"=> "towerofgodt01.jpg",
            "prix" => "7.35"

        ]);


        Livre::create([
            "stock"=> 7,
            "nom"=> "Tower of God Tome 02",
            "imageURL"=> "towerofgodt02.jpg",
            "prix" => "7.35"

        ]);

        Livre::create([
            "stock"=> 10,
            "nom"=> "Tower of God Tome 03",
            "imageURL"=> "towerofgodt03.jpg",
            "prix" => "7.35"

        ]);

        Livre::create([
            "stock"=> 13,
            "nom"=> "Tower of God Tome 04",
            "imageURL"=> "towerofgodt04.jpg",
            "prix" => "7.35"

        ]);

        Livre::create([
            "stock"=> 6,
            "nom"=> "Tower of God Tome 05",
            "imageURL"=> "towerofgodt05.jpg",
            "prix" => "7.35"

        ]);

        Livre::create([
            "stock"=> 3,
            "nom"=> "Tower of God Tome 06",
            "imageURL"=> "towerofgodt06.jpg",
            "prix" => "7.35"

        ]);





        /* TAL*/

        Livre::create([
            "stock"=> 13,
            "nom"=> "Tal Tome 01",
            "imageURL"=> "talt01.jpg",
            "prix" => "7.45"

        ]);


        Livre::create([
            "stock"=> 7,
            "nom"=> "Tal Tome 02",
            "imageURL"=> "talt01.jpg",
            "prix" => "7.45"

        ]);

        Livre::create([
            "stock"=> 10,
            "nom"=> "Tal Tome 03",
            "imageURL"=> "talt01.jpg",
            "prix" => "7.45"

        ]);

        Livre::create([
            "stock"=> 13,
            "nom"=> "Tal Tome 04",
            "imageURL"=> "talt01.jpg",
            "prix" => "7.45"

        ]);

        Livre::create([
            "stock"=> 6,
            "nom"=> "Tal Tome 05",
            "imageURL"=> "talt01.jpg",
            "prix" => "7.45"

        ]);

        Livre::create([
            "stock"=> 3,
            "nom"=> "Tal Tome 06",
            "imageURL"=> "talt01.jpg",
            "prix" => "7.45"

        ]);

        Livre::create([
            "stock"=> 7,
            "nom"=> "Tal Tome 07",
            "imageURL"=> "talt01.jpg",
            "prix" => "7.45"

        ]);

        Livre::create([
            "stock"=> 4,
            "nom"=> "Tal Tome 08",
            "imageURL"=> "talt01.jpg",
            "prix" => "7.45"

        ]);

        Livre::create([
            "stock"=> 5,
            "nom"=> "Tal Tome 09",
            "imageURL"=> "talt01.jpg",
            "prix" => "7.45"

        ]);

        Livre::create([
            "stock"=> 9,
            "nom"=> "Tal Tome 10",
            "imageURL"=> "talt01.jpg",
            "prix" => "7.45"

        ]);

        /* Overgeared */

        Livre::create([
            "stock"=> 13,
            "nom"=> "Overgeared Tome 01",
            "imageURL"=> "overgearedt01.jpg",
            "prix" => "7.45"

        ]);


        Livre::create([
            "stock"=> 7,
            "nom"=> "Overgeared Tome 02",
            "imageURL"=> "overgearedt01.jpg",
            "prix" => "7.45"

        ]);

        Livre::create([
            "stock"=> 10,
            "nom"=> "Overgeared Tome 03",
            "imageURL"=> "overgearedt01.jpg",
            "prix" => "7.45"

        ]);

        Livre::create([
            "stock"=> 13,
            "nom"=> "Overgeared Tome 04",
            "imageURL"=> "overgearedt01.jpg",
            "prix" => "7.45"

        ]);

        Livre::create([
            "stock"=> 6,
            "nom"=> "Overgeared Tome 05",
            "imageURL"=> "overgearedt01.jpg",
            "prix" => "7.45"

        ]);

        Livre::create([
            "stock"=> 3,
            "nom"=> "Overgeared Tome 06",
            "imageURL"=> "overgearedt01.jpg",
            "prix" => "7.45"

        ]);

        Livre::create([
            "stock"=> 7,
            "nom"=> "Overgeared Tome 07",
            "imageURL"=> "overgearedt01.jpg",
            "prix" => "7.45"

        ]);

        Livre::create([
            "stock"=> 4,
            "nom"=> "Overgeared Tome 08",
            "imageURL"=> "overgearedt01.jpg",
            "prix" => "7.45"

        ]);

        Livre::create([
            "stock"=> 5,
            "nom"=> "Overgeared Tome 09",
            "imageURL"=> "overgearedt01.jpg",
            "prix" => "7.45"

        ]);

        Livre::create([
            "stock"=> 9,
            "nom"=> "Overgeared Tome 10",
            "imageURL"=> "overgearedt01.jpg",
            "prix" => "7.45"

        ]);

        /* Dalsez */

        Livre::create([
            "stock"=> 13,
            "nom"=> "Dalsez Tome 01",
            "imageURL"=> "dalsezt01.jpg",
            "prix" => "7.45"

        ]);


        Livre::create([
            "stock"=> 7,
            "nom"=> "Dalsez Tome 02",
            "imageURL"=> "dalsezt01.jpg",
            "prix" => "7.45"

        ]);

        Livre::create([
            "stock"=> 10,
            "nom"=> "Dalsez Tome 03",
            "imageURL"=> "dalsezt01.jpg",
            "prix" => "7.45"

        ]);

        Livre::create([
            "stock"=> 13,
            "nom"=> "Dalsez Tome 04",
            "imageURL"=> "dalsezt01.jpg",
            "prix" => "7.45"

        ]);

        Livre::create([
            "stock"=> 6,
            "nom"=> "Dalsez Tome 05",
            "imageURL"=> "dalsezt01.jpg",
            "prix" => "7.45"

        ]);

        Livre::create([
            "stock"=> 3,
            "nom"=> "Dalsez Tome 06",
            "imageURL"=> "dalsezt01.jpg",
            "prix" => "7.45"

        ]);

        Livre::create([
            "stock"=> 7,
            "nom"=> "Dalsez Tome 07",
            "imageURL"=> "dalsezt01.jpg",
            "prix" => "7.45"

        ]);

        Livre::create([
            "stock"=> 4,
            "nom"=> "Dalsez Tome 08",
            "imageURL"=> "dalsezt01.jpg",
            "prix" => "7.45"

        ]);

        Livre::create([
            "stock"=> 5,
            "nom"=> "Dalsez Tome 09",
            "imageURL"=> "dalsezt01.jpg",
            "prix" => "7.45"

        ]);

        Livre::create([
            "stock"=> 9,
            "nom"=> "Dalsez Tome 10",
            "imageURL"=> "dalsezt01.jpg",
            "prix" => "7.45"

        ]);

        $commande1 = Commande::create([
            "etat" => "en preparation",
            "user_id" => "$moi->id"
        ]);

        $ligne1= Ligne::create([
            "commande_id"=> $commande1->id,
            "livre_id" => $livre1->id,
            "quantite" =>2

        ]);

        $ligne2 = Ligne::create([
            "commande_id"=> $commande1->id,
            "livre_id" => $livre2->id,
            "quantite" =>4

        ]);
    }
}